import { Component, OnInit } from '@angular/core';
import { Todo } from './../todo';
import { Storage } from './../storage';

@Component({
	selector: 'app-list',
	templateUrl: './list.component.html',
    styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

    Todo : Todo = new Todo();
    Storage : Storage = new Storage();
    items = this.Storage.get('todo') || [];
    completed : number;

    /**
     * Method class constructor
     *
     * Starts methods and properties for the class
     *
     * @constructor
     * @return {void}
     */
	constructor() {}

    /**
     * Adds a new task to the list
     *
     * @private
     * @memberof ListComponent
     * @return {void}
    */
    addTodo() {
        if(this.Todo.name === '') return;
        // atualiza a lista
        this.items.push(this.Todo);
        // atualiza o banco
		this.Storage.set(this.items);
		// reset
		this.Todo = new Todo();
	}

    /**
     * Removes a completed task from the list
     *
     * @private
     * @memberof ListComponent
     * @return {void}
    */
	removeTodo(id) {
        this.items = this.items.filter((item) => item.id != id);
		this.Storage.remove(id);
    }

    /**
     * Clear the list
     *
     * @private
     * @memberof ListComponent
     * @return {void}
    */
    clearAll() {
        this.items = [];
        this.Storage.setList(this.items);
    }

    /**
     * Remove only the marked as complete
     *
     * @private
     * @memberof ListComponent
     * @return {void}
    */
	removeAllCompleted() {
        this.items = this.items.filter((item) => item.complete == false);
        this.Storage.setList(this.items);
	}

	ngOnInit() {

    }

}
