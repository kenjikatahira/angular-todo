import {Util} from './util';

export class Todo {
    name : string = '';
    id : string = new Util().uniq();
    complete : boolean = false;

    constructor(values: Object = {}) {
        Object.assign(this,values)
    }
}
