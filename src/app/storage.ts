export class Storage {
    storage : string;
    ls = window.localStorage;

    /**
     * Method class constructor
     *
     * Starts methods and properties for the class
     *
     * @constructor
     * @memberof Storage
     * @return {void}
     */
    constructor(storage : string = 'todo') {
        this.storage = storage;
    }

    /**
     * Set the data to local storage
     *
     * @set
     * @memberof Storage
     * @return {void}
     */
    set(value : Object = {}) {
        if(!this.get(this.storage)) {
            this.ls.setItem(this.storage, JSON.stringify([value]));
        } else {
            this.ls.setItem(this.storage,JSON.stringify(value));
        }
    }

    /**
     * Remove a task from the list on local storage
     *
     * @remove
     * @memberof Storage
     * @return {void}
     */
    remove(id : string) {
        console.log(`${id} apagado!`);
        const list = this.get(this.storage).filter((item) => id != item.id);
        this.ls.setItem(this.storage,JSON.stringify(list));
    }

    /**
     * Override all the data from the local storage
     *
     * @setList
     * @memberof Storage
     * @return {void}
     */
    setList(value : Object) {
        this.ls.setItem(this.storage,JSON.stringify(value));
    }

    /**
     * Retrieve the data from local storage
     *
     * @get
     * @memberof Storage
     * @return {void}
     */
    get(key : string = this.storage) {
        return JSON.parse(this.ls.getItem(key));
    }
}
